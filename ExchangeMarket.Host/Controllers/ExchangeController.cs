﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExchangeMarket.Services;
using Microsoft.AspNetCore.Mvc;

namespace ExchangeMarket.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExchangeController : ControllerBase
    {
        private readonly IExchangeHistoryService _exchangeHistoryService;

        public ExchangeController(IExchangeHistoryService exchangeHistoryService)
        {
            _exchangeHistoryService = exchangeHistoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetHistory([FromQuery] int month, int year, string outType)
        {
            if (outType == "json")
            {
                return Ok(await _exchangeHistoryService.GetExchangeHistoryJsonAsync(month, year));
            }
            else if (outType == "txt")
            {
                return Ok(await _exchangeHistoryService.GetExchangeHistoryTxtAsync(month, year));
            }

            return BadRequest();
        }
    }
}
