﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ExchangeMarket.Services;
using ExchangeMarket.Services.Impl;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;

namespace ExchangeMarket.ConsoleApp
{
    public class Program
    {
        public static IServiceCollection ServiceCollection { get; private set; }

        public static void Main(string[] args)
        {
            CancellationTokenSource cts = new CancellationTokenSource();

            Console.CancelKeyPress += (s, e) =>
            {
                e.Cancel = true;
                cts.Cancel();
            };

            try
            {
                var task = MainAsync(args);
                task.Wait(cts.Token);
                Environment.Exit(task.Result);
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR:" + ex);
                Environment.Exit(1);
            }
        }

        public static IServiceProvider ConfigureServices()
        {
            var configuration = GetConfiguration();

            var services = new ServiceCollection()
                .AddScoped<ISeedService, SeedService>()
                .AddSingleton<ICnbParsingService, CnbParsingService>();

            services.AddSingleton(e => configuration);

            var appSettings = new AppSettings();
            configuration.Bind("AppSettings", appSettings);
            services.AddSingleton(appSettings);

            services.AddDbContext<AppContext>(options => options.UseMySql(appSettings.Connection));

            ServiceCollection = services;

            var serviceProvider = services.BuildServiceProvider();

            return serviceProvider;
        }

        private static IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        private static async Task<int> MainAsync(string[] args)
        {
            var exitCode = 0;

            try
            {
                var serviceProvider = ConfigureServices();

                var taskType = serviceProvider.GetService<AppSettings>().AppType;

                if (taskType == "console")
                {
                    await serviceProvider.GetService<ICnbParsingService>().PushYearRates();
                }
                else
                {
                    await serviceProvider.GetService<ICnbParsingService>().PushDailyRates(DateTime.Now);
                }
            }
            catch (MySqlException)
            {
                Console.WriteLine("Database is not ready. Try again later");
                exitCode = 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                exitCode = 1;
            }

            return exitCode;
        }
    }
}
