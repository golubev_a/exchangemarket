CREATE DATABASE  IF NOT EXISTS `cnb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `cnb`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cnb
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Currencies`
--

DROP TABLE IF EXISTS `Currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Currencies` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Country` longtext,
  `CurrencyName` longtext,
  `Code` longtext,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Currencies`
--

LOCK TABLES `Currencies` WRITE;
/*!40000 ALTER TABLE `Currencies` DISABLE KEYS */;
INSERT INTO `Currencies` VALUES (1,'Australia','dollar','AUD'),(2,'Turkey','lira','TRY'),(3,'Thailand','baht','THB'),(4,'Switzerland','franc','CHF'),(5,'Sweden','krona','SEK'),(6,'South Korea','won','KRW'),(7,'South Africa','rand','ZAR'),(8,'Singapore','dollar','SGD'),(9,'Russia','rouble','RUB'),(10,'Romania','new leu','RON'),(11,'Poland','zloty','PLN'),(12,'Philippines','peso','PHP'),(13,'Norway','krone','NOK'),(14,'New Zealand','dollar','NZD'),(15,'Mexico','peso','MXN'),(16,'United Kingdom','pound','GBP'),(17,'Malaysia','ringgit','MYR'),(18,'Israel','shekel','ILS'),(19,'Indonesia','rupiah','IDR'),(20,'India','rupee','INR'),(21,'IMF','SDR','XDR'),(22,'Iceland','krona','ISK'),(23,'Hungary','forint','HUF'),(24,'Hongkong','dollar','HKD'),(25,'EMU','euro','EUR'),(26,'Denmark','krone','DKK'),(27,'Croatia','kuna','HRK'),(28,'China','renminbi','CNY'),(29,'Canada','dollar','CAD'),(30,'Bulgaria','lev','BGN'),(31,'Brazil','real','BRL'),(32,'Japan','yen','JPY'),(33,'USA','dollar','USD');
/*!40000 ALTER TABLE `Currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rates`
--

DROP TABLE IF EXISTS `Rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Rates` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RateDate` date NOT NULL,
  `Amount` int(11) NOT NULL,
  `Rate` double NOT NULL,
  `CurrencyId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_Rates_CurrencyId` (`CurrencyId`),
  CONSTRAINT `FK_Rates_Currencies_CurrencyId` FOREIGN KEY (`CurrencyId`) REFERENCES `Currencies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rates`
--

LOCK TABLES `Rates` WRITE;
/*!40000 ALTER TABLE `Rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-30 20:34:43
