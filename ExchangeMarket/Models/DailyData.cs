﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket.Models
{
    public class DailyData
    {
        public string Country { get; set; }

        public string Currency { get; set; }

        public string Amount { get; set; }

        public string Code { get; set; }

        public string Rate { get; set; }
    }
}
