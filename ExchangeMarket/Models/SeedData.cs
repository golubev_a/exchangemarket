﻿using ExchangeMarket.Models.Db;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket.Models
{
    public class SeedData
    {
        public List<Currency> Currencies { get; set; }
    }
}
