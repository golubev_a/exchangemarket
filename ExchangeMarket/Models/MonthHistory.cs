﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExchangeMarket.Models.Db;

namespace ExchangeMarket.Models
{
    public class MonthHistory
    {
        public int Year { get; set; }

        public string Month { get; set; }

        public List<Period> WeekPeriods { get; set; }

        public MonthHistory()
        {
            WeekPeriods = new List<Period>();
        }
    }

    public class Period
    {
        public string WeekDates { get; set; }

        public List<HistoryCurrencyRate> CurrencyRates { get; set; }

        public Period()
        {
            CurrencyRates = new List<HistoryCurrencyRate>();
        }
    }

    public class HistoryCurrencyRate
    {
        public string Currency { get; set; }

        public double Max { get; set; }

        public double Min { get; set; }

        public double Median
        {
            get
            {
                return Math.Round((Max + Min) / 2, 6);
            }
        }

        public HistoryCurrencyRate(List<CurrencyRate> currencies)
        {
            var currency = currencies.FirstOrDefault().Currency;
            var amount = currencies.FirstOrDefault().Amount;
            Currency = currency.Code;
            Max = currencies.Select(t => t.Rate).Max() / amount;
            Min = currencies.Select(t => t.Rate).Min() / amount;
        }
    }
}
