﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket.Models.Db
{
    public class Currency
    {
        public int Id { get; set; }

        public string Country { get; set; }

        public string CurrencyName { get; set; }

        public string Code { get; set; }

        public List<CurrencyRate> Rates { get; set; }
    }
}
