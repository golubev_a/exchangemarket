﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ExchangeMarket.Models.Db
{
    public class CurrencyRate
    {
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime RateDate { get; set; }

        public int Amount { get; set; }

        public double Rate { get; set; }

        public int CurrencyId { get; set; }

        public Currency Currency { get; set; }
    }
}
