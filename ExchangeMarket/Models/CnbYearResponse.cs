﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket.Models
{
    public class CnbYearResponse
    {
        public IEnumerable<YearData> YearData { get; set; }
    }
}
