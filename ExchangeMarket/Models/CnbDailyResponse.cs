﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket.Models
{
    public class CnbDailyResponse
    {
        public string Date { get; set; }

        public IEnumerable<DailyData> DailyData { get; set; }
    }
}
