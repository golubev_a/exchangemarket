﻿using ExchangeMarket.Models;
using System.Threading.Tasks;

namespace ExchangeMarket.Services
{
    public interface IExchangeHistoryService
    {
        Task<MonthHistory> GetExchangeHistoryJsonAsync(int month, int year);

        Task<string> GetExchangeHistoryTxtAsync(int month, int year);
    }
}