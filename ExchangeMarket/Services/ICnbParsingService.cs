﻿using System;
using System.Threading.Tasks;
using ExchangeMarket.Models;

namespace ExchangeMarket.Services
{
    public interface ICnbParsingService
    {
        Task PushDailyRates(DateTime rateDate);

        Task PushYearRates();
    }
}