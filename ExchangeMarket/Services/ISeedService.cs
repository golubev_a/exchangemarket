﻿using System.Threading.Tasks;

namespace ExchangeMarket.Services
{
    public interface ISeedService
    {
        void Seed();
    }
}