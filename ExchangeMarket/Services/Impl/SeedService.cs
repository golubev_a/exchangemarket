﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExchangeMarket.Models;
using ExchangeMarket.Models.Db;
using Newtonsoft.Json;

namespace ExchangeMarket.Services.Impl
{
    public class SeedService : ISeedService
    {
        private readonly AppContext _appContext;

        public SeedService(AppContext appContext)
        {
            _appContext = appContext;
        }

        public void Seed()
        {
            var dbCreated = _appContext.Database.EnsureCreated();
            if (dbCreated)
            {
                using (var file = File.OpenText(@"./seedData.json"))
                {
                    var serializer = new JsonSerializer();
                    var currencies = (SeedData)serializer.Deserialize(file, typeof(SeedData));

                    _appContext.Currencies.AddRange(currencies.Currencies);
                    
                    _appContext.SaveChanges();
                }
            }
        }
    }
}
