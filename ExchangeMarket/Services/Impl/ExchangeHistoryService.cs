﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ExchangeMarket.Extensions;
using ExchangeMarket.Models;
using ExchangeMarket.Models.Db;

namespace ExchangeMarket.Services.Impl
{
    public class ExchangeHistoryService : IExchangeHistoryService
    {
        private readonly AppContext _appContext;
        private readonly AppSettings _apiSettings;

        public ExchangeHistoryService(AppContext appContext, AppSettings apiSettings)
        {
            _appContext = appContext;
            _apiSettings = apiSettings;
        }

        public async Task<string> GetExchangeHistoryTxtAsync(int month, int year)
        {
            var monthHistory = await GetExchangeHistory(month, year);

            return monthHistory.ToTxt();
        }

        public async Task<MonthHistory> GetExchangeHistoryJsonAsync(int month, int year)
        {
            return await GetExchangeHistory(month, year);
        }

        private async Task<MonthHistory> GetExchangeHistory(int month, int year)
        {
            var currencies = _apiSettings.Currencies.Split(',');
            var rates = await _appContext
                .Rates
                .Include(t => t.Currency)
                .AsNoTracking()
                .Where(tt => tt.RateDate.Month == month && tt.RateDate.Year == year && currencies.Any(t => t == tt.Currency.Code))
                .OrderBy(t => t.RateDate)
                .ToListAsync();

            var currencyList = new List<CurrencyRate>();
            int weekNumber = 0;
            var tmpRates = new List<TmpRateList>();
            TmpRateList tmpRate = null;

            for (int i = 0; i < rates.Count; i++)
            {
                var rateWeekNumber = rates[i].RateDate.GetIso8601WeekOfYear();

                if (weekNumber != rateWeekNumber || i == rates.Count - 1)
                {
                    var rateWeekStartDay = i == 0 ? rates[i].RateDate : rates[i].RateDate.StartOfWeek();
                    var rateWeekEndDay = rates[i].RateDate.StartOfWeek().AddDays(6);

                    weekNumber = rateWeekNumber;

                    if (i == rates.Count - 1)
                    {
                        tmpRate.Rates.Add(rates[i]);
                        tmpRate.WeekDatesPeriod = $"{rateWeekStartDay.Day}...{rates[i].RateDate.Day}";
                    }

                    if (tmpRate != null)
                    {
                        tmpRates.Add(tmpRate);
                    }

                    tmpRate = new TmpRateList
                    {
                        WeekNumber = weekNumber,
                        WeekDatesPeriod = $"{rateWeekStartDay.Day}...{rateWeekEndDay.Day}"
                    };
                }

                tmpRate.Rates.Add(rates[i]);
            }

            var monthHistory = new MonthHistory
            {
                Month = CultureInfo.CreateSpecificCulture("en").DateTimeFormat.GetMonthName(month),
                Year = year
            };

            foreach (var rate in tmpRates)
            {
                var period = new Period
                {
                    WeekDates = rate.WeekDatesPeriod
                };

                foreach (var currency in currencies)
                {
                    period.CurrencyRates.Add(new HistoryCurrencyRate(rate.Rates.Where(t => t.Currency.Code == currency).ToList()));
                }

                monthHistory.WeekPeriods.Add(period);
            }

            return monthHistory;
        }

        internal class TmpRateList
        {
            public int WeekNumber { get; set; }

            public string WeekDatesPeriod { get; set; }

            public List<CurrencyRate> Rates { get; set; }

            public TmpRateList()
            {
                Rates = new List<CurrencyRate>();
            }
        }
    }
}
