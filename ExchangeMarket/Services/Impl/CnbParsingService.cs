﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using ExchangeMarket.Models;
using ExchangeMarket.Models.Db;
using Microsoft.EntityFrameworkCore;
using RestEase;

namespace ExchangeMarket.Services.Impl
{
    public class CnbParsingService : ICnbParsingService
    {
        private readonly ICnbApi _cnbApi;
        private readonly AppContext _appContext;
        private readonly AppSettings _appSettings;

        private const string cnbUrl = "https://www.cnb.cz";

        public CnbParsingService(AppContext appContext, AppSettings appSettings)
        {
            _cnbApi = RestClient.For<ICnbApi>(cnbUrl);
            _appContext = appContext;
            _appSettings = appSettings;
        }

        public async Task PushDailyRates(DateTime rateDate)
        {
            Console.WriteLine($"Daily rates updating for {rateDate.ToShortDateString()} started...");

            var requestDate = rateDate.ToShortDateString();

            var response = await _cnbApi.GetDailyRate(requestDate);

            using (var stream = StreamFromString(response.StringContent))
            {
                using (TextReader textReader = new StreamReader(stream))
                {
                    var csvReader = new CsvReader(textReader);
                    csvReader.Configuration.Delimiter = "|";
                    csvReader.Configuration.MissingFieldFound = null;

                    csvReader.Read();

                    var currencies = _appContext
                        .Currencies
                        .AsNoTracking()
                        .ToList();

                    var rates = csvReader
                        .GetRecords<DailyData>()
                        .ToList()
                        .Select(t => new CurrencyRate
                        {
                            Amount = int.Parse(t.Amount),
                            Rate = double.Parse(t.Rate),
                            RateDate = rateDate,
                            CurrencyId = currencies.FirstOrDefault(tt => tt.Code == t.Code).Id
                        })
                        .ToList();

                    await InsertRates(rates);
                }
            }

            Console.WriteLine("Rates updating finished");
        }

        public async Task PushYearRates()
        {
            Console.WriteLine($"Rates updating for {_appSettings.Years} started...");
            var years = _appSettings.Years.Split(',');
            foreach (var year in years)
            {
                await ExecuteYearRates(int.Parse(year));
            }

            Console.WriteLine("Rates updating finished");
        }

        async Task ExecuteYearRates(int year)
        {
            var response = await _cnbApi.GetYearRate(year);

            using (var stream = StreamFromString(response.StringContent))
            {
                using (TextReader textReader = new StreamReader(stream))
                {
                    var csvReader = new CsvReader(textReader);
                    csvReader.Configuration.Delimiter = "|";
                    csvReader.Configuration.MissingFieldFound = null;

                    var records = csvReader.GetRecords<dynamic>().ToList();

                    if (records.Count == 0)
                    {
                        return;
                    }

                    var currencies = _appContext
                        .Currencies
                        .AsNoTracking()
                        .ToList();

                    var yearRates = new List<CurrencyRate>();

                    foreach (IDictionary<string, object> row in records)
                    {
                        var rateDate = new DateTime();

                        foreach (var col in row)
                        {
                            var columnName = col.Key;
                            var columnValue = (string)col.Value;

                            if (columnName == "Date")
                            {
                                rateDate = DateTime.ParseExact(columnValue, "dd.MMM yyyy", null);
                            }
                            else
                            {
                                var currencyRate = new CurrencyRate();

                                var amountNcode = columnName.Split(' ');

                                var currency = currencies.FirstOrDefault(t => t.Code == amountNcode[1]);

                                if (currency != null)
                                {
                                    yearRates.Add(new CurrencyRate
                                    {
                                        Amount = int.Parse(amountNcode[0]),
                                        CurrencyId = currency.Id,
                                        Rate = double.Parse(columnValue),
                                        RateDate = rateDate
                                    });
                                }
                            }
                        }
                    }

                    await InsertRates(yearRates);
                }
            }
        }

        async Task InsertRates(List<CurrencyRate> rates)
        {
            var dbRates = await _appContext.Rates.AsNoTracking().ToListAsync();

            rates.RemoveAll(tt => dbRates.Any(xx => xx.CurrencyId == tt.CurrencyId && xx.RateDate.ToShortDateString() == tt.RateDate.ToShortDateString()));

            await _appContext
                        .Rates
                        .AddRangeAsync(rates);

            await _appContext.SaveChangesAsync();
        }

        static Stream StreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
