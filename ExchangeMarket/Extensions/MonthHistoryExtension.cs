﻿using ExchangeMarket.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket.Extensions
{
    public static class MonthHistoryExtension
    {
        public static string ToTxt(this MonthHistory h)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"Year: {h.Year}, month: {h.Month}")
                .AppendLine()
                .AppendLine("Week periods:");

            foreach (var period in h.WeekPeriods)
            {
                stringBuilder.AppendLine()
                    .Append($"{period.WeekDates}: ");

                foreach (var currency in period.CurrencyRates)
                {
                    stringBuilder.Append($"{currency.Currency} - max: {currency.Max}, min: {currency.Min}, median: {currency.Median}; ");
                }

                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }
    }
}
