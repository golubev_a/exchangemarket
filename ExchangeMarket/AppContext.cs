﻿using ExchangeMarket.Models.Db;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket
{
    public class AppContext : DbContext
    {
        public DbSet<Currency> Currencies { get; set; }

        public DbSet<CurrencyRate> Rates { get; set; }

        public AppContext(DbContextOptions<AppContext> options)
            : base(options)
        {
        }
    }
}
