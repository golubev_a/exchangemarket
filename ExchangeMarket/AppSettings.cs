﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeMarket
{
    public class AppSettings
    {
        public string Currencies { get; set; }

        public string Years { get; set; }

        public string Connection { get; set; }

        public string AppType { get; set; }
    }
}
