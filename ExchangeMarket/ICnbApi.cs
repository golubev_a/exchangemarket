﻿using ExchangeMarket.Models;
using RestEase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeMarket
{
    [AllowAnyStatusCode]
    public interface ICnbApi
    {
        [Get("en/financial_markets/foreign_exchange_market/exchange_rate_fixing/daily.txt")]
        Task<Response<string>> GetDailyRate(string date);

        [Get("en/financial_markets/foreign_exchange_market/exchange_rate_fixing/year.txt")]
        Task<Response<string>> GetYearRate(int year);
    }
}
